<?php

// COMPOSER AUTOLOAD
require_once 'vendor/autoload.php';

// MOCK
$mock = $alunos = array(
	array("nome" => "João", "serie" => "3º Ano"),
	array("nome" => "Pedro", "serie" => "3º Ano"),
	array("nome" => "Vitor", "serie" => "3º Ano"),
	array("nome" => "Ricardo", "serie" => "3º Ano"),
);

for ($i = 0; $i < 2000; $i++) {
	foreach ($mock as $key => $value) {
		array_push($alunos, $mock[$key]);
	}
}

// var_dump($alunos);

// Instanciamos a classe
$objPHPExcel = new PHPExcel();

// Criamos as colunas
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nome do Aluno')
            ->setCellValue('B1', "Série ")
            ->setCellValue("C1", "Turma");

$objValidation = $objPHPExcel->getActiveSheet()->getCell('C2')->getDataValidation();
$objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
$objValidation->setAllowBlank(false);
$objValidation->setShowInputMessage(true);
$objValidation->setShowErrorMessage(true);
$objValidation->setShowDropDown(true);
$objValidation->setErrorTitle('Erro.');
$objValidation->setError('Este valor não está na lista.');
$objValidation->setPromptTitle('Selecione da lista.');
$objValidation->setPrompt('Por favor escolha um valor da lista');
$objValidation->setFormula1('"Turma A,Turma B,Turma C"');

$linha = 2;

foreach ($alunos as $key => $aluno) {

	$objPHPExcel->getActiveSheet()->setCellValue("A" . $linha, $aluno['nome']);
	$objPHPExcel->getActiveSheet()->setCellValue("B" . $linha, $aluno['serie']);
	$objPHPExcel->getActiveSheet()->getCell("C" . $linha)->setDataValidation(clone $objValidation);

	$linha++;

}

// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
$objPHPExcel->getActiveSheet()->setTitle('Nome da Planilha');

// Colunas com tamanho automático
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

// Cabeçalho do arquivo para ele baixar
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="selecao-de-turmas.xls"');
header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
header('Cache-Control: max-age=1');

// Acessamos o 'Writer' para poder salvar o arquivo
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Salva diretamente no output, poderíamos mudar aqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
$objWriter->save('php://output');

exit;

?>